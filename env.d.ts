/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_GOOGLE_MAPS_KEY: string;
  readonly VITE_OPEN_WEATHER_KEY: string;
  // more env variables...
}

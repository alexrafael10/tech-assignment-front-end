import { defineStore } from "pinia";

export type PreferencesState = {
  useLocationServices: boolean;
  showMapPreview: boolean;
};

const STORAGE_KEY = "preferences";

export const usePreferencesStore = defineStore({
  id: "preferences",
  state: (): PreferencesState => ({
    useLocationServices: true,
    showMapPreview: true,
  }),
  actions: {
    loadPreferences() {
      const preferences: PreferencesState = JSON.parse(
        localStorage.getItem(STORAGE_KEY) ?? "{}"
      );

      this.useLocationServices = preferences.useLocationServices;
      this.showMapPreview = preferences.showMapPreview;
    },

    persistPreferences(preferences: PreferencesState) {
      const { useLocationServices, showMapPreview } = preferences;
      localStorage.setItem(
        STORAGE_KEY,
        JSON.stringify({ useLocationServices, showMapPreview })
      );
    },
  },
});

import {
  getCurrentWeather,
  getHistoricalWeather,
  getWeatherForecast,
} from "@/api";
import type {
  Coordinates,
  LocationInformation,
  UnixTimestamp,
  WeatherInfo,
} from "@/types";
import { dateToUnixTimestamp, setDateDay } from "@/utils";
import { defineStore } from "pinia";

export type WeatherState = {
  currentWeather: WeatherInfo | null;
  weatherForecast: WeatherInfo[];
  historicalWeather: { [k: string]: WeatherInfo };
};

export const useWeatherStore = defineStore({
  id: "weather",
  state: (): WeatherState => ({
    currentWeather: null,
    weatherForecast: [],
    historicalWeather: {},
  }),
  getters: {
    allHistoricalWeather: (state) => {
      return Object.keys(state.historicalWeather)
        .sort((a, b) => parseInt(a) - parseInt(b))
        .map((key) => state.historicalWeather[key]);
    },
    todaysHighLowTemperature: (state) => {
      const todaysWeather = state.weatherForecast[0];
      const { maxTemp, minTemp } = todaysWeather || {};
      return { maxTemp, minTemp };
    },
  },
  actions: {
    async loadCurrentWeather(coordinates: Coordinates) {
      this.currentWeather = await getCurrentWeather(coordinates);
    },
    async loadWeatherForecast(coordinates: Coordinates) {
      this.weatherForecast = await getWeatherForecast(coordinates);
    },
    async loadHistoricalWeather(coordinates: Coordinates, daysAgo: number) {
      const dateAgo = setDateDay(new Date(), -daysAgo);
      const daysAgoTimestamp = dateToUnixTimestamp(dateAgo);
      this.historicalWeather[daysAgo] = await getHistoricalWeather(
        coordinates,
        daysAgoTimestamp
      );
    },
  },
});

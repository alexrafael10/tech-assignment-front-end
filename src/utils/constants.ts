export const ONE_CALL_API = "https://api.openweathermap.org/data/2.5/onecall";

export const OPEN_WEATHER_HISTORICAL_URL = ONE_CALL_API + "/timemachine";

export const OPEN_WEATHER_ICON_URL = "https://openweathermap.org/img/wn";

export const UNITS = {
  wind: { metric: "km/h", imperial: "mph" },
  pressure: { metric: "mb", imperial: "mb" },
};

import type { WeatherInfo } from "@/types";
import type { OWTempInformation, OWWeatherInfo } from "@/types/open-weather";

export const toQueryParams = (targetObject?: any) => {
  if (!targetObject) return "";

  return Object.keys(targetObject).reduce((acc, key) => {
    const curr = targetObject[key];
    if (curr !== undefined) {
      const prefix = acc ? "&" : "?";
      acc += `${prefix}${key}=${curr}`;
    }

    return acc;
  }, "");
};

export const setDateDay = (date: Date, daysDifference: number) => {
  date.setDate(date.getDate() + daysDifference);
  return date;
};

export const dateToUnixTimestamp = (date: Date) => {
  return Math.floor(date.getTime() / 1000);
};

export const formatTimeLocal = (unixTimestamp: number) => {
  return new Intl.DateTimeFormat("en-us", {
    hour12: true,
    hour: "2-digit",
    minute: "2-digit",
  }).format(new Date(unixTimestamp * 1000));
};

export const formatDateLocal = (unixTimestamp: number, timezone?: string) => {
  return new Intl.DateTimeFormat("en-us", {
    weekday: "long",
    month: "short",
    day: "numeric",
  }).format(new Date(unixTimestamp * 1000));
};

export const mpsToKmph = (mps: number) => 3.6 * mps;

export const gatherWeatherInfo = (
  weatherInfo: OWWeatherInfo,
  temp?: OWTempInformation
): WeatherInfo => {
  const {
    wind_speed: windSpeed,
    humidity,
    pressure,
    sunrise: sunriseTime,
    sunset: sunsetTime,
    weather,
    dt: dataTime,
  } = weatherInfo;
  const currentTemp =
    typeof weatherInfo.temp === "number"
      ? weatherInfo.temp
      : weatherInfo.temp?.day ?? temp?.day;
  return {
    temp: currentTemp as number,
    maxTemp: temp?.max ?? (weatherInfo.temp as OWTempInformation).max,
    minTemp: temp?.min ?? (weatherInfo.temp as OWTempInformation).min,
    weather,

    windSpeed,
    humidity,
    pressure,
    sunriseTime,
    sunsetTime,

    dataTime,
  };
};

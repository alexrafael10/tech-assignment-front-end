import type { Coordinates, UnixTimestamp } from "@/types";
import type {
  OWExclude,
  OWUnit,
  OWQueryParams,
  OWWeatherResponse,
} from "@/types/open-weather";
import { gatherWeatherInfo, toQueryParams } from "@/utils";
import { ONE_CALL_API, OPEN_WEATHER_HISTORICAL_URL } from "@/utils/constants";

export const getResource = async <T>(URL: string): Promise<T> => {
  const response = await fetch(URL);
  return response.json();
};

export const getWeather = async (
  coordinates: Coordinates,
  options?: { units?: OWUnit; type?: "current" | "daily" }
) => {
  const exclude: OWExclude[] = [];

  if (options?.type === "current") {
    exclude.push("daily");
  }

  if (options?.type === "daily") {
    exclude.push("current");
  }

  const params: OWQueryParams = {
    appid: import.meta.env.VITE_OPEN_WEATHER_KEY,
    lat: coordinates.lat,
    lon: coordinates.lon,
    units: options?.units || "metric",
    exclude: [...exclude, "minutely", "hourly", "alerts"],
  };

  return getResource<OWWeatherResponse>(ONE_CALL_API + toQueryParams(params));
};

export const getCurrentWeather = async (coordinates: Coordinates) => {
  const response = await getWeather(coordinates, { type: "current" });
  return gatherWeatherInfo(response.current);
};

export const getWeatherForecast = async (coordinates: Coordinates) => {
  const response = await getWeather(coordinates, { type: "daily" });
  return response.daily.map((d) => gatherWeatherInfo(d));
};

export const getHistoricalWeather = async (
  coordinates: Coordinates,
  date: UnixTimestamp,
  options?: { units: OWUnit }
) => {
  const params: OWQueryParams = {
    appid: import.meta.env.VITE_OPEN_WEATHER_KEY,
    dt: date,
    lat: coordinates.lat,
    lon: coordinates.lon,
    units: options?.units || "metric",
    only_current: "{true}",
  };
  const { current } = await getResource<OWWeatherResponse>(
    OPEN_WEATHER_HISTORICAL_URL + toQueryParams(params)
  );

  return gatherWeatherInfo(current);
};

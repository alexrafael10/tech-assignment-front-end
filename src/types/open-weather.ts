import type { UnixTimestamp } from ".";

export type OWUnit = "metric" | "imperial";

export type OWExclude = "current" | "minutely" | "hourly" | "daily" | "alerts";

export type OWBoolean = "{true}";

export type OWQueryParams = {
  appid: string;
  lat: number;
  lon: number;
  units: OWUnit;
  exclude?: OWExclude[];
  only_current?: OWBoolean;
  dt?: UnixTimestamp;
};

export interface OWWeatherItem {
  id: number;
  description: string;
  icon: string;
  main: string;
}

export type OWTempInformation = { min: number; max: number; day: number };

export interface OWWeatherInfo {
  humidity: number;
  pressure: number;
  sunrise: UnixTimestamp;
  sunset: UnixTimestamp;
  temp: number | OWTempInformation;
  weather: [OWWeatherItem];
  wind_speed: number;
  dt: UnixTimestamp;
}

// Api returns more, but only defining the ones i'm using for simplicity
export interface OWWeatherResponse {
  current: OWWeatherInfo & { temp: number };
  daily: (OWWeatherInfo & { temp: OWTempInformation })[];
}

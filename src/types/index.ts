import type { OWWeatherItem } from "./open-weather";

export interface WeatherInfo {
  weather: [OWWeatherItem];
  temp: number;
  maxTemp: number;
  minTemp: number;

  windSpeed: number;
  humidity: number;
  pressure: number;
  sunriseTime: number;
  sunsetTime: number;

  dataTime: UnixTimestamp;
}

export type Coordinates = {
  lat: number;
  lon: number;
};

export type UnixTimestamp = number;

export type LocationInformation = {
  name: string;
  coordinates: Coordinates;
};

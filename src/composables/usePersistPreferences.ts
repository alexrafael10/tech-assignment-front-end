import { usePreferencesStore } from "@/stores/preferences";
import { storeToRefs } from "pinia";
import { onBeforeMount } from "vue";

export const usePersistPreferences = () => {
  const preferencesStore = usePreferencesStore();

  preferencesStore.$subscribe((mutation, state) => {
    preferencesStore.persistPreferences(state);
  });

  onBeforeMount(() => {
    preferencesStore.loadPreferences();
  });

  return storeToRefs(preferencesStore);
};

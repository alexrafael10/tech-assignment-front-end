import type { Coordinates } from "@/types";
import { onBeforeMount, ref } from "vue";

export const useGeocoder = () => {
  const geocoder = ref<google.maps.Geocoder | null>(null);

  onBeforeMount(async () => {
    geocoder.value = new google.maps.Geocoder();
  });

  // getLocation
  const getCoordinatesFromSearch = async (addressString: string) => {
    const response = await geocoder.value?.geocode({ address: addressString });
    if (!response?.results) return;

    const firstResult = response.results[0];
    const { lat, lng } = firstResult.geometry.location;
    return {
      lat: lat(),
      lon: lng(),
    };
  };

  const getCityFromCoordinates = async (coordinates: Coordinates) => {
    const latLng = new google.maps.LatLng(coordinates.lat, coordinates.lon);
    const response = await geocoder.value?.geocode({ location: latLng });
    if (!response?.results) return "";

    const { formatted_address: formattedAddress } =
      response.results.find(({ types }) =>
        types.includes("administrative_area_level_1")
      ) ?? {};

    if (!formattedAddress) return "";

    return formattedAddress;
  };

  return {
    getCoordinatesFromSearch,
    getCityFromCoordinates,
  };
};

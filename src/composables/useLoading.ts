import { ref } from "vue";

export const useLoading = () => {
  const loading = ref(true);
  const error = ref(false);

  const load = async (loadCallback: () => Promise<void>) => {
    try {
      loading.value = true;
      error.value = false;
      await loadCallback();
    } catch (err) {
      console.error(err);
      error.value = true;
    } finally {
      loading.value = false;
    }
  };

  return {
    loading,
    error,
    load,
  };
};

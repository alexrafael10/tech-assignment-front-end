import type { Coordinates, LocationInformation } from "@/types";
import { onBeforeMount, ref } from "vue";
import { useGeocoder } from "./useGeocoder";

export const useCurrentLocation = () => {
  const currentUserLocation = ref<LocationInformation | null>(null);
  const { getCityFromCoordinates } = useGeocoder();

  const getCurrentLocationInformation = () => {
    return new Promise<LocationInformation | null>((resolve, reject) => {
      const geolocationOptions = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
      };

      const successCallback = async (position: GeolocationPosition) => {
        const { longitude: lon, latitude: lat } = position.coords;
        const locationName = await getCityFromCoordinates({ lat, lon });
        resolve({ name: locationName, coordinates: { lat, lon } });
      };

      const errorCallback = (error: any) => {
        console.error(error);
        reject(null);
      };

      navigator.geolocation.getCurrentPosition(
        successCallback,
        errorCallback,
        geolocationOptions
      );
    });
  };

  const loadCurrentUserLocation = async () => {
    try {
      currentUserLocation.value = await getCurrentLocationInformation();
    } catch {
      // reset current location, user most likely declined location request
      currentUserLocation.value = null;
    }
  };

  return {
    currentUserLocation,
    loadCurrentUserLocation,
  };
};

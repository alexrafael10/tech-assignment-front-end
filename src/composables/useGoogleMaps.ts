import type { Coordinates } from "@/types";
import { onMounted, ref, watch, type Ref } from "vue";

export const useGoogleMaps = (coordinates: Ref<Coordinates>) => {
  const mapElementRef = ref<HTMLDivElement | null>(null);

  const map = ref<google.maps.Map | null>(null);
  const marker = ref<google.maps.Marker | null>(null);

  const initMap = (coordinates: Coordinates) => {
    const { lat, lon: lng } = coordinates;

    map.value = new google.maps.Map(mapElementRef.value as HTMLDivElement, {
      zoom: 7,
      center: { lat, lng },
      disableDefaultUI: true,
      gestureHandling: "none",
    });

    marker.value = new google.maps.Marker({
      position: { lat, lng },
      map: map.value,
    });
  };

  watch(coordinates, initMap);

  onMounted(() => {
    initMap(coordinates.value);
  });

  return {
    mapElementRef,
  };
};

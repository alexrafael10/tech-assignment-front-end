import type { Coordinates } from "@/types";
import { onMounted, ref } from "vue";

export type PlacePrediction = google.maps.places.AutocompletePrediction;

export const usePlacesAutocomplete = () => {
  const autocompleteService =
    ref<google.maps.places.AutocompleteService | null>(null);

  const getPlacePredictions = (searchString: string) => {
    return new Promise<PlacePrediction[]>((resolve, reject) => {
      autocompleteService.value?.getPlacePredictions(
        {
          input: searchString,
          types: ["(cities)"],
        },
        (predictions, status) => {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            resolve(predictions ?? []);
          } else {
            reject(status);
          }
        }
      );
    });
  };

  onMounted(async () => {
    autocompleteService.value = new google.maps.places.AutocompleteService();
  });

  return {
    getPlacePredictions,
  };
};
